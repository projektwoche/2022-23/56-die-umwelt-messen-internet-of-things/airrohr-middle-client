data = type(e).__name__ + ": " + str(e)
import network_setup
import secrets
import api

network_setup.do_connect(network_setup.wlan, secrets.wifi_ssid, secrets.wifi_key)
api.sendStatus(secrets.api_endpoint, secrets.api_token, secrets.api_esp_id, 1, data)
