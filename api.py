import urequests
def sendData(endpoint, api_token, api_esp_id, temperature, humidity, pm10, pm25):
    header = {"Content-Type": "application/json", "X-API-TOKEN": api_token}
    post_data = {
        "sensor_id": api_esp_id,
        "temperature": temperature,
        "humidity": humidity,
        "pm10": pm10,
        "pm25": pm25,
    }
    res = urequests.post(endpoint+"?type=data", headers=header, json=post_data)
    print(res.text)
    res.close()


def sendStatus(endpoint, api_token, api_esp_id, status_code, data):
    header = {"Content-Type": "application/json", "X-API-TOKEN": api_token}
    post_data = {"sensor_id": api_esp_id, "statuscode": status_code, "data": data}
    res = urequests.post(endpoint+"?type=status", headers=header, json=post_data)
    print(res.text)
    res.close()
