def check_pm_data(data):
    if type(data) != bytes:
        print("failed on type")
        return False
    if len(data) != 10:
        print("failed on length")
        return False
    if data[:2] != b"\xaa\xc0":
        print("failed on header")
        return False
    if data[9:] != b"\xab":
        print("failed on tail")
        return False
    if (data[2] + data[3] + data[4] + data[5] + data[6] + data[7]) % 256 != data[8]:
        print(
            "failed on checksum: checksum:",
            data[8],
            "computed:",
            data[2] + data[3] + data[4] + data[5] + data[6] + data[7],
        )
        return False
    return True


def get_pm_data(pm_sensor):
    data = pm_sensor.read()
    while data == None:
        data = pm_sensor.read()
    while True:
        while True:
            f = data.find(b"\xaa\xc0")
            if f == -1:
                data = data[-10:]
            else:
                data = data[f:]
            if len(data) >= 10 and f != -1:
                break
            read_data = pm_sensor.read()
            while read_data == None:
                read_data = pm_sensor.read()
            data += read_data
        if check_pm_data(data[:10]):
            return data[:10]
        else:
            print(type(data), data)
            data = data[2:]


def calc_pm_data(data):
    return ((data[2] + (256 * data[3])) / 10, (data[4] + (256 * data[5])) / 10)

def get(pm_sensor):
    return calc_pm_data(get_pm_data(pm_sensor))