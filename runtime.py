import network
import dht
import time
from machine import UART
import json
import sds011
import api
import secrets

api.sendStatus(
    secrets.api_endpoint,
    secrets.api_token,
    secrets.api_esp_id,
    0,
    "boot",
)

# get sensor objects
pm_sensor = UART(0, baudrate=9600)
dht_sensor = dht.DHT22(machine.Pin(secrets.dht22_pin_hw))

while True:
    pm_data = sds011.get(pm_sensor)
    time.sleep(secrets.api_rate)
    pm25 = pm_data[0]  # pm2.5 data
    pm10 = pm_data[1]  # pm10 data
    dht_sensor.measure()
    print(  # debug print data
        "temperature:",
        dht_sensor.temperature(),
        "humidity:",
        dht_sensor.humidity(),
        "pm2.5:",
        pm25,
        "pm10:",
        pm10,
    )
    api.sendData(
        secrets.api_endpoint,
        secrets.api_token,
        secrets.api_esp_id,
        dht_sensor.temperature(),
        dht_sensor.humidity(),
        pm10,
        pm25,
    )
