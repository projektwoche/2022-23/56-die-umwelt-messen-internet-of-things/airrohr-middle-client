import secrets
import time
import os

exec(open("network_setup.py").read())

do_connect(wlan, secrets.wifi_ssid, secrets.wifi_key)
time.sleep(1)  # give time to CTRL-C on UART-0/USB
os.dupterm(None, 1)
