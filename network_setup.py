import network


def do_connect(wlan, ssid, key):
    if not wlan.isconnected():
        print("connecting to network...")
        wlan.connect(ssid, key)
        while not wlan.isconnected():
            pass
    print("network config:", wlan.ifconfig())
    print("mac adress:", "".join([hex(e)[2:] for e in wlan.config("mac")]).upper())


wlan = network.WLAN(network.STA_IF)
wlan.active(True)
