try:
    import setup

    exec(open("runtime.py").read())
except KeyboardInterrupt:
    pass  # alow ctrl-C escape while setup
except Exception as e:
    try:
        exec(open("error_runtime.py").read())
    except:
        pass
    import machine

    machine.reset()
